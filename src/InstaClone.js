import React, {Component} from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import Login from './components/screens/Login';
import Camera from './components/screens/Camera';
import Profile from './components/screens/Profile';
import MainFeed from './components/screens/MainFeed';
import Register from './components/screens/Register';
import Gallery from './components/screens/Gallery';
import { StackNavigator, SwitchNavigator, TabNavigator } from 'react-navigation';

const Tabs = TabNavigator({
    main: MainFeed,
    profile:Profile,
    camera:Camera,
    gallery:Gallery
}, {
    tabBarPosition: 'bottom',
  });

const IntroStack = StackNavigator({
    login: Login,
    register: Register
})

const MainStack =  SwitchNavigator({
    login: IntroStack,
    main: Tabs
});

class InstaClone extends Component {
  
    render() {
        return (
            <MainStack />
        );
    }
}

export default InstaClone;
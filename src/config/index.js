export default {
    images: {
        heartIcon: require("../../assets/heart.png"),
        chatIcon: require("../../assets/bubble.png"),
        arrowIcon: require("../../assets/arrow.png")
    },
    styleConstants: {
        rowHeight: 50
    },
    baseUrl: 'http://instaapi-dpzsoj.turbo360-vertex.com/api/'
    //baseUrl: 'http://localhost:3000/api/'
};
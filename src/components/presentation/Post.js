import React, {Component} from 'react'
import {View, Text, StyleSheet, Image, Dimensions, TouchableOpacity} from 'react-native'
import config from "../../config";

class Post extends Component {
  
    constructor(){
        super();
        this.state = {
            liked: false,
            screenWidth: Dimensions.get("window").width
        };
    }

    likeToggled() {
        this.setState({
            liked:!this.state.liked
        })
    }

    componentWillMount() {
     this.setState({
         screenWidth:  Dimensions.get("window").width
     });
    }
  
    render() {
        const imageUri = "https://lh3.googleusercontent.com/S_vryhjV7jsrIc914yonHuQLs-_YZM0jDrHClwHRlHNEd5NIzYEvOuFqhiR7Us4ERQmJXc3z8ke-y8wbSsu8rfXZ=s1024" + "=s" + Math.floor(this.state.screenWidth * 1.1) + "-c";
        const imageSelection = this.props.id % 2 == 0 ? "https://lh3.googleusercontent.com/-MwjsZyAL7fasS0KGFcXnMK6reI_EckCMbo1rMkwfOXKyKJ4U6a5qN8j3j-dm_MLODyEHDCUcGPPxHNt9dYT-QG3Nec=s1024" : "https://lh3.googleusercontent.com/kO2stAEBhq4bKISh2sWzPK-I2VqOSdBwrDsXYZYzzh9szOpHpQ4H-vKFxgticLuOvekUlxWeUPHq-ikzKxE9yLNH=s1024";
        const heartIconColor = (this.state.liked) ? 'rgb(252,61,57)' : null;
        return (
            <View style={{ flex:1, width: 100 + "%",  height: 100 + "%"}}>
                <View style={styles.useBar}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        style={styles.userPic}
                        source={{uri:"https://lh3.googleusercontent.com/rarI4Bu-KjBB3jZLpJlNTVR--hh540AO7_LzBsCHtdI4Ox4LkDlgxexJYU55_G7BP6wQYWdnBHkZ9cy-11P-i213R5g=s84-c"}}
                        />
                        <Text style={{marginLeft:10}}>Mohammad Zulqurnain</Text> 
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <Text style={{ fontSize:30, marginRight:10}}>...</Text> 
                    </View>
                </View>
                <TouchableOpacity onPress={()=>{
                   this.likeToggled();
                }}>
                <Image                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                    style={{ width: this.state.screenWidth, height:400}}
                    source={{uri:imageSelection}}
                />
                </TouchableOpacity>
               <View style={styles.iconBar}>
                    <Image style={[
                        styles.icon, 
                        {height: 40, width: 40, tintColor: heartIconColor}]} 
                        source={config.images.heartIcon} />
                    <Image style={[styles.icon, {height: 36, width: 36}]} 
                        source={config.images.chatIcon} />
                    <Image 
                        resizeMode="stretch" 
                        style={[styles.icon, {height: 50, width: 40}]} 
                        source={config.images.arrowIcon} />
               </View>
               <View style={styles.iconBar}>
                    <Image style={[
                        styles.icon, 
                        {height: 20, width: 20}]} 
                        source={config.images.heartIcon} />
                    <Text>128 Likes</Text>
               </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    tempNav: {
        width: 100 + "%",
        height: 56,
        marginTop: 20,
        backgroundColor: "rgb(250, 250, 250)",
        borderBottomColor: "rgb(233, 233, 233)",
        justifyContent: "center",
        alignItems: "center",
    },
    useBar: {
        width: 100 + "%",
        height: config.styleConstants.rowHeight,
        backgroundColor: "rgb(255, 255, 255)",
        flexDirection: "row",
        marginHorizontal: 10,
        //marginTop:-50,
        justifyContent: "space-between"
    },
    userPic: {
        height: 40,
        width: 40,
        borderRadius: 20 
    },
    userPic: {
        height: 40,
        width: 40,
        borderRadius: 20 
    },
    iconBar: {
        height: config.styleConstants.rowHeight,
        width: 100 + "%",
        borderColor: "rgb(233,233,233)",
        borderTopWidth: StyleSheet.hairlineWidth,
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection:"row",
        alignItems: "center"
    },
    icon: {
        marginLeft: 5
    }
});

export default Post;
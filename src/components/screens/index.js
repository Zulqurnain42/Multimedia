import MainFeed from './MainFeed';
import Login from "./Login";
import Profile from "./Profile";
import Camera from "./Camera";
import Register from "./Register";
import Gallery from "./Gallery";
export { MainFeed, Login, Profile, Camera, Gallery, Register };
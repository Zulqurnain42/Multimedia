import React, { Component } from 'react';
import { View, Text, TouchableOpacity,TextInput, Button, StyleSheet } from 'react-native';
import config from "../../config";

class Register extends Component {

	constructor() {
	   super()
	   this.state = {
		   credentials: {
			   email: "",
			   password: "",
			   confirmPassword: ""
		   }
	   };
	}

	updateText(text, field) {
	   newCredentials = Object.assign(this.state.credentials)
	   newCredentials[field] = text;
	   this.setState({
		   credentials: newCredentials
	   })
	}

	navigateToMain(){
		this.props.navigation.navigate('main');
	  //Navigate  to Main App
	 }
 
	 register(){
		
		if(this.state.password == this.state.confirmPassword) {
		fetch(config.baseUrl + 'signup', {
			method: 'POST',
			headers: {
			  Accept: 'application/json',
			  'Content-Type': 'application/json',
			},
			body: JSON.stringify(this.state.credentials)
		  }).then(response => response.json())
		  .then(jsonResponse => {
			  if(jsonResponse.confirmation === "success"){
				  alert("User successfully created");
				  this.props.navigation.navigate("login");
			  }else{
				alert("User already exist please make different account");
			  }
		  })
		  .catch((err)=> {
			  console.log(err.message);
		  })
		}else {
			alert("Passwords does not match.");
		}
	 }
 
	render(){ 
		return ( 
			<View 
			 style={{height:100+'%', 
			 width:100+'%', 
			 flex:1, 
			 justifyContent:"center",
		 	 alignItems:"center",
			 backgroundColor: "rgb(252, 61, 57)"
			}}
			>
			<Text>REGISTER PAGE</Text>
			<TextInput 
			    value={this.state.login}
			    onChangeText={text => this.updateText(text, "email")}
				placeholder="Username"
				auttoCorrect={false}
			    style={styles.input} />
			<TextInput 
			    value={this.state.password}
			    onChangeText={text => this.updateText(text, "password")}
			    secureTextEntry 
				placeholder="Password" 
				auttoCorrect={false}
			    style={styles.input} 
			   />
			<TextInput 
			    value={this.state.confirmPassword}
			    onChangeText={text => this.updateText(text, "confirmPassword")}
			    secureTextEntry 
				placeholder="Confirm Password" 
				auttoCorrect={false}
			    style={styles.input} 
			   />
		   <Button onPress={() =>
				 {this.register();
				 }}
				  title="Signup"
			   />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	input: {
		height: 50,
		width: 100 + "%",
		marginHorizontal: 50,
		backgroundColor: "rgb(255, 255, 255)",
		marginBottom: 10
	}
});

export default Register;
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import Header from '../controls/Header';
import Bar from '../controls/Bar';
import PhotoGrid from '../controls/PhotoGrid';

class Profile extends Component {

    render(){ 
        return ( 
           <View style={styles.container}>
                <Header />
                <Bar />
                <PhotoGrid />
           </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
      flex: 1,
      backgroundColor:"#0394c0"
    }
});

export default Profile;
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import PhotoGrid from '../controls/PhotoGrid';

class Gallery extends Component {

    render(){ 
        return ( 
           <View style={styles.container}>
                <PhotoGrid />
           </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
      flex: 1,
      backgroundColor:"#0394c0"
    }
});

export default Gallery;
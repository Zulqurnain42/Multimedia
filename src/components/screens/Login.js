import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Button } from 'react-native';
import config from "../../config";

class Login extends Component {

	constructor() {
        super()
        this.state = {
            credentials: {
                email: "",
                password: ""
            }
        };
     }
 
     updateText(text, field) {
        newCredentials = Object.assign(this.state.credentials)
        newCredentials[field] = text;
        this.setState({
            credentials: newCredentials
        })
     }
 
     navigateToMain(){
         this.props.navigation.navigate('main');
       //Navigate  to Main App
      }
  
      login() {
         fetch(config.baseUrl + 'login', {
             method: 'POST',
             headers: {
               Accept: 'application/json',
               'Content-Type': 'application/json',
             },
             body: JSON.stringify(this.state.credentials)
           }).then(response => response.json())
           .then(jsonResponse => {

               console.log(JSON.stringify(jsonResponse));
               
               if(jsonResponse.confirmation === "success"){
                   //alert("Success");
                   this.props.navigation.navigate("main");
               }else {
                   alert("User with these credentials do not exist. Please enter correct credentials.");
               }
           })
           .catch((err)=> {
            console.log(JSON.stringify(err));
           })
      }
  
     render(){ 
         return ( 
             <View 
              style={{height:100+'%', 
              width:100+'%', 
              flex:1, 
              justifyContent:"center",
               alignItems:"center",
              backgroundColor: "rgb(252, 61, 57)"
             }}
             >
             <Text>REGISTER PAGE</Text>
             <TextInput 
                 value={this.state.login}
                 onChangeText={text => this.updateText(text, "email")}
                 placeholder="Username"
                 auttoCorrect={false}
                 style={styles.input} />
             <TextInput 
                 value={this.state.password}
                 onChangeText={text => this.updateText(text, "password")}
                 secureTextEntry 
                 placeholder="Password" 
                 auttoCorrect={false}
                 style={styles.input} 
                />
            <Button onPress={() =>
                  {this.login();
                  }}
                   title="Login"
                />
            <Button onPress={() =>
               this.props.navigation.navigate('register')}
                title="No Account? Sign up here!"
            />
             </View>
         )
     }
 }
 
 const styles = StyleSheet.create({
     input: {
         height: 50,
         width: 100 + "%",
         marginHorizontal: 50,
         backgroundColor: "rgb(255, 255, 255)",
         marginBottom: 10
     }
 });
 
export default Login;
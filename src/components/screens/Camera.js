import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
} from 'react-native';
//import { RNCamera, FaceDetector } from 'react-native-camera';

export default class Camera extends Component {
 
 render() {
   return (<View style={styles.container}>
              {/* <RNCamera 
              ref={(cam) => {
                this.camera = cam
              }}
              style={styles.view}
              aspect={Camera.constants.Aspect.aspect}
              >
              <Text
              style={styles.capture}
              onPress={this.takePicture.bind(this)}>
              [CAPTURE_IMAGE]
              </Text>
              </RNCamera> */}
          </View>
          );
 }
  
  takePicture() {
    alert('In takePicture!');
    const options = {}
    this.camera.capture({metadata:options}).then((data) => {
      console.log(data)
    }).catch((error) => {
      console.log(error)
    })
  }

}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'row'
  },
  view: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: 'steelblue',
    borderColor: 'steelblue',
    color: 'red',
    padding: 15,
    margin: 45,
    
  }

});
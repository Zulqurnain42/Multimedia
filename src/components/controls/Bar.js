import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text
} from 'react-native';

export default class Bar extends Component {
    render() {
        return (
            <View style={styles.Bar}>
                <View style={[styles.barIte, styles.barseparator]}>
                    <Text style={styles.barBottom}>Following</Text>
                    <Text style={styles.barTop}>12K</Text>
                </View>
                <View style={styles.barItem}>
                    <Text style={styles.barBottom}>Followers</Text>
                    <Text style={styles.barTop}>32K</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bar: {
        borderTopColor: '#fff',
        borderTopWidth: 4,
        backgroundColor: '#ec2e4a',
        flexDirection: 'row'
    },
    barseparator: {
        borderRightWidth: 4
    },
    barTop: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        fontStyle: 'italic'

    },
    barBottom: {
        color: '#000',
        fontSize:14,
        fontWeight: 'bold',
    }
});
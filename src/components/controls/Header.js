import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground
} from 'react-native';

export default class Header extends Component {
    render() {
        return (
            <ImageBackground style={styles.headerBackground} source={require('../../../images/headerbg.jpg')}>
                <View style={styles.header}>
                    <View style={styles.profilepicWrap}>
                        <Image style={styles.profilepic} source={require('../../../images/profilepic.jpg')} />
                    </View>
                    <Text style={styles.name}>Mohammad Zulqurnain</Text>
                    <Text style={styles.pos}> - APP DEVELOPER - </Text>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        width: null,
        alignSelf: 'stretch',
    },
    headerBackground: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    profilepicWrap: {
        width:180,
        height: 180,
        borderRadius: 100,
        borderColor: 'rgba(0,0,0,0.4)',
        borderWidth: 16,
    },
    profilepic: {
        flex:1,
        marginTop:-5,
        width: null,
        alignSelf: 'stretch',
        borderRadius: 100,
        borderColor: '#fff',
        borderWidth: 4
    },
    name: {
        marginTop:-2,
        fontSize: 16,
        color: '#fff',
        fontWeight: 'bold'
    },
    pos: {
        marginTop:0,
        fontSize:14,
        color: '#fff'
    }
});